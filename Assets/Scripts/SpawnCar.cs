﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class SpawnCar : MonoBehaviour
{
    public GameObject[] cars;
    public int nun;
    public Random numb;
    public bool newcar = true;


    void Start()
    {

        nun = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (newcar == true)
        {
            StartCoroutine(Spawn());

        }
    }
    IEnumerator Spawn()
    {
        Debug.Log("unity");
        newcar = false;
        yield return new WaitForSeconds(2);
        nun = UnityEngine.Random.Range(1, 4);
        Debug.LogWarning(nun);
        switch (nun)
        {
            case 1:
                Instantiate(cars[0], cars[0].transform.position, Quaternion.identity);
                break;
            case 2:
                Instantiate(cars[1], cars[1].transform.position, Quaternion.identity);
                break;
            case 3:
                Instantiate(cars[2], cars[2].transform.position, Quaternion.identity);
                break;
            case 4:
                Instantiate(cars[3], cars[3].transform.position, Quaternion.identity);
                break;
        }
        yield return new WaitForSeconds(1);
        newcar = true;

    }
}
