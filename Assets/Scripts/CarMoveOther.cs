﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarMoveOther : MonoBehaviour
{
    public bool move = true;
    public float speed = 2f;
    public GameObject thes;
    
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (move == true)
        {
            
            transform.Translate(Vector3.left * speed * Time.deltaTime);
            
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Respawn")
        {
            Debug.Log("killed");
            Destroy(thes);
            
            
        }
        
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
                
            Debug.Log("Сбили");
            SceneManager.LoadScene("SampleScene");


        }
        else if(other.gameObject.tag == "Respawn") Debug.Log("Игрок пошёл не туда");
    }
}
