﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.Collections.Generic;
using InfinityCode.uContext.Windows;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace InfinityCode.uContext.PopupWindows
{
    public class Hierarchy : PopupWindowItem
    {
        public override Texture icon
        {
            get { return EditorIconContents.hierarchyWindow.image; }
        }

        protected override string label
        {
            get { return "Hierarchy"; }
        }

        public override float order
        {
            get { return -80; }
        }

        private static void FinalizeWindow(EditorWindow window)
        {
            Type windowType = EditorTypes.sceneHierarchyWindow;

            if (Selection.activeGameObject != null)
            {
                Reflection.InvokeMethod(windowType, "SetExpandedRecursive", window,
                    new[] {typeof(int), typeof(bool)},
                    new object[] {Selection.activeGameObject.GetInstanceID(), true});
            }
            else
            {
                object sceneHierarchy = Reflection.GetFieldValue(windowType, "m_SceneHierarchy", window);
                Reflection.InvokeMethod(EditorTypes.sceneHierarchy, "SetScenesExpanded", sceneHierarchy, new[] {typeof(List<string>)}, new[] {new List<string> {SceneManager.GetActiveScene().name}});
            }
        }

        protected override void ShowTab(Vector2 mousePosition)
        {
            Type windowType = EditorTypes.sceneHierarchyWindow;
            Vector2 windowSize = Prefs.contextMenuWindowSize;
            Rect rect = new Rect(GUIUtility.GUIToScreenPoint(mousePosition) - windowSize / 2, Vector2.zero);

            Rect windowRect = new Rect(rect.position, windowSize);

            EditorWindow window = ScriptableObject.CreateInstance(windowType) as EditorWindow;
            window.Show();
            window.Focus();
            window.position = windowRect;

            FinalizeWindow(window);
        }

        protected override void ShowUtility(Vector2 mousePosition)
        {
            Type windowType = EditorTypes.sceneHierarchyWindow;
            Vector2 windowSize = Prefs.contextMenuWindowSize;
            Rect rect = new Rect(GUIUtility.GUIToScreenPoint(mousePosition) - windowSize / 2, Vector2.zero);

            Rect windowRect = new Rect(rect.position, windowSize);

            EditorWindow window = ScriptableObject.CreateInstance(windowType) as EditorWindow;
            window.ShowUtility();
            window.Focus();
            window.position = windowRect;

            FinalizeWindow(window);
        }

        protected override void ShowDropDown(Vector2 mousePosition)
        {
            Type windowType = EditorTypes.sceneHierarchyWindow;
            Vector2 windowSize = Prefs.contextMenuWindowSize;
            Rect rect = new Rect(GUIUtility.GUIToScreenPoint(mousePosition) - windowSize / 2, Vector2.zero);

            Rect windowRect = new Rect(rect.position, windowSize);
            if (windowRect.y < 40) windowRect.y = 40;

            EditorWindow window = ScriptableObject.CreateInstance(windowType) as EditorWindow;
            window.position = windowRect;
            window.ShowPopup();
            window.Focus();
            EventManager.AddBinding(EventManager.ClosePopupEvent).OnInvoke += b =>
            {
                window.Close();
                b.Remove();
            };
            PinAndClose.Show(window, windowRect, window.Close, () =>
            {
                EditorWindow wnd = Object.Instantiate(window);
                wnd.Show();
                wnd.position = window.position;
                wnd.maxSize = new Vector2(4000f, 4000f);
                wnd.minSize = new Vector2(100f, 100f);
                window.Close();
            });

            FinalizeWindow(window);
        }
    }
}