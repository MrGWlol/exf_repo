﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Tools
{
    [InitializeOnLoad]
    public static class TerrainBrushSize
    {
        private static bool isActive = false;
        private static Terrain terrain;
        private static FieldInfo sizeField;
        private static PropertyInfo selectedToolProp;
        private static FieldInfo activeInstanceField;

#if UNITY_2019_1_OR_NEWER
        private static PropertyInfo treeBrushSizeField;
        private static Type paintTreeToolType;
        private static PropertyInfo paintTreeInstanceProp;
#else 
        private static FieldInfo treeBrushSizeField;
#endif

        static TerrainBrushSize()
        {
            try
            {
                Selection.selectionChanged += OnSelectionChanged;
                activeInstanceField = EditorTypes.terrainInspector.GetField("s_activeTerrainInspectorInstance", BindingFlags.Static | BindingFlags.NonPublic);
                sizeField = EditorTypes.terrainInspector.GetField("m_Size", BindingFlags.Instance | BindingFlags.NonPublic);
                selectedToolProp = EditorTypes.terrainInspector.GetProperty("selectedTool", BindingFlags.NonPublic | BindingFlags.Instance);

#if UNITY_2019_1_OR_NEWER
                paintTreeToolType = Reflection.GetEditorType("Experimental.TerrainAPI.PaintTreesTool");
                treeBrushSizeField = paintTreeToolType.GetProperty("brushSize", BindingFlags.Instance | BindingFlags.Public);
                Type ssType = typeof(ScriptableSingleton<>);
                Type[] typeArgs = { paintTreeToolType };
                paintTreeToolType = ssType.MakeGenericType(typeArgs);
                paintTreeInstanceProp = paintTreeToolType.GetProperty("instance", BindingFlags.Static | BindingFlags.Public);
#else
                treeBrushSizeField = EditorTypes.treePainter.GetField("brushSize", BindingFlags.Static | BindingFlags.Public);
#endif

                OnSelectionChanged();
            }
            catch (Exception e)
            {
                Log.Add(e);
            }
            
        }

        private static void OnSceneGUI(SceneView sceneview)
        {
            if (!isActive || terrain == null) return;
            if (Preview.isActive) return;

            Event e = Event.current;
            if (e.type != EventType.ScrollWheel || e.modifiers != Prefs.terrainBrushSizeModifiers && e.modifiers != Prefs.terrainBrushSizeBoostModifiers) return;

            Editor editor = activeInstanceField.GetValue(null) as Editor;
            if (editor == null) return;

            int value = (int) selectedToolProp.GetValue(editor, new object[0]);

            float size;
#if UNITY_2019_1_OR_NEWER
            object instance = paintTreeInstanceProp.GetValue(null, null);
            if (value == 2) size = (float)treeBrushSizeField.GetValue(instance, null);
#else
            if (value == 1) size = (float) treeBrushSizeField.GetValue(null);
#endif
            else size = (float) sizeField.GetValue(editor);

            float delta = e.delta.y;
            if (e.modifiers == Prefs.terrainBrushSizeBoostModifiers) delta *= 10;
            size = Mathf.Clamp(size + delta, 0.1f, Mathf.Round(Mathf.Min(terrain.terrainData.size.x, terrain.terrainData.size.z) * 15f / 16f));

#if UNITY_2019_1_OR_NEWER
            if (value == 2) treeBrushSizeField.SetValue(instance, size, null);
#else
            if (value == 1) treeBrushSizeField.SetValue(null, size);
#endif
            else sizeField.SetValue(editor, size);

            e.Use();
        }

        private static void OnSelectionChanged()
        {
            if (!Prefs.terrainBrushSize) return;

            isActive = false;
            terrain = null;
            SceneViewManager.RemoveListener(OnSceneGUI);

            if (Selection.activeGameObject == null) return;
            GameObject go = Selection.activeGameObject;
            terrain = go.GetComponent<Terrain>();
            if (terrain == null) return;

            SceneViewManager.AddListener(OnSceneGUI);
            isActive = true;
        }
    }
}