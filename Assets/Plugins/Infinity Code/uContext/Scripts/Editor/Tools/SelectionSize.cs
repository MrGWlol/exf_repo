﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Tools
{
    [InitializeOnLoad]
    public static class SelectionSize
    {
        public const string StyleID = "sv_label_6";
        private static GUIStyle style;
        private static bool forcedShow;

        static SelectionSize()
        {
            SceneViewManager.AddListener(OnSceneViewGUI, 0, true);
        }

        public static void SetState(bool value)
        {
            forcedShow = value;
        }

        private static void OnSceneViewGUI(SceneView sceneView)
        {
            if (!Event.current.capsLock && !forcedShow) return;
            if (!SelectionBoundsManager.hasBounds) return;
            if (SelectionBoundsManager.renderers[0].gameObject.scene.name == null) return;

            if (style == null)
            {
                style = new GUIStyle(StyleID)
                {
                    fontSize = 10,
                    alignment = TextAnchor.MiddleCenter,
                    wordWrap = false,
                    fixedHeight = 16,
                    normal =
                    {
                        textColor = Color.white
                    },
                    padding = new RectOffset(),
                    stretchWidth = true
                };
            }

            Color color = Handles.color;
            Handles.color = Color.red;

            Bounds bounds = SelectionBoundsManager.bounds;
            Vector3 min = bounds.min;
            Vector3 max = bounds.max;

            Vector3 p1 = min;
            Vector3 p2 = new Vector3(max.x, min.y, min.z);
            Vector3 p3 = new Vector3(min.x, max.y, min.z);
            Vector3 p4 = new Vector3(min.x, min.y, max.z);
            Vector3 p5 = new Vector3(max.x, max.y, min.z);
            Vector3 p6 = new Vector3(max.x, min.y, max.z);
            Vector3 p7 = new Vector3(min.x, max.y, max.z);
            Vector3 p8 = max;

            Handles.DrawLine(p1, p2);
            Handles.DrawLine(p1, p3);
            Handles.DrawLine(p2, p5);
            Handles.DrawLine(p3, p5);

            Handles.DrawLine(p4, p6);
            Handles.DrawLine(p4, p7);
            Handles.DrawLine(p6, p8);
            Handles.DrawLine(p7, p8);

            Handles.DrawLine(p1, p4);
            Handles.DrawLine(p2, p6);
            Handles.DrawLine(p3, p7);
            Handles.DrawLine(p5, p8);

            Vector3 d1 = p1 + p2;
            Vector3 d2 = p3 + p5;
            Vector3 d3 = p7 + p8;
            Vector3 d4 = p4 + p6;

            Vector3 d5 = p1 + p3;
            Vector3 d6 = p2 + p5;
            Vector3 d7 = p4 + p7;
            Vector3 d8 = p6 + p8;

            Vector3 d9 = p1 + p4;
            Vector3 d10 = p2 + p6;
            Vector3 d11 = p3 + p7;
            Vector3 d12 = p5 + p8;

            string m1 = (p1 - p2).magnitude.ToString("F2");
            string m2 = (p1 - p3).magnitude.ToString("F2");
            string m3 = (p1 - p4).magnitude.ToString("F2");

            Handles.Label(d1 / 2, m1, style);
            Handles.Label(d2 / 2, m1, style);
            Handles.Label(d3 / 2, m1, style);
            Handles.Label(d4 / 2, m1, style);

            Handles.Label(d5 / 2, m2, style);
            Handles.Label(d6 / 2, m2, style);
            Handles.Label(d7 / 2, m2, style);
            Handles.Label(d8 / 2, m2, style);

            Handles.Label(d9 / 2, m3, style);
            Handles.Label(d10 / 2, m3, style);
            Handles.Label(d11 / 2, m3, style);
            Handles.Label(d12 / 2, m3, style);

            Handles.color = color;
        }
    }
}