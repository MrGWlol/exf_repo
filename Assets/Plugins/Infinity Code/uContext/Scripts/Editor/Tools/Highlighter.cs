﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace InfinityCode.uContext.Tools
{
    [InitializeOnLoad]
    public static class Highlighter
    {
        private static GameObject hoveredGO;
        private static Dictionary<SceneView, SceneViewOutline> viewDict;
        private static Renderer[] lastRenderers;
        private static GameObject[] _lastGameObjects;

        public static GameObject[] lastGameObjects
        {
            get { return _lastGameObjects; }
        }

        static Highlighter()
        {
            HierarchyItemDrawer.Register("Highlighter", DrawHierarchyItem, 0);
            EditorApplication.modifierKeysChanged += RepaintAllHierarchies;
            EditorApplication.update += EditorUpdate;
            viewDict = new Dictionary<SceneView, SceneViewOutline>();
        }

        private static Rect DrawHierarchyItem(int id, Rect rect)
        {
            if (!Prefs.highlight) return rect;

            Event e = Event.current;
            GameObject go = EditorUtility.InstanceIDToObject(id) as GameObject;

            if (Prefs.highlightOnHierarchy) HighlightOnHierarchy(rect, e, go);

            return rect;
        }

        private static void EditorUpdate()
        {
            var wnd = EditorWindow.mouseOverWindow;
            if (wnd != null && wnd.GetType() == EditorTypes.sceneHierarchyWindow && !wnd.wantsMouseMove)
            {
                wnd.wantsMouseMove = true;
            }
        }

        public static bool Highlight(GameObject go)
        {
            Renderer[] renderers = null;
            if (go != null) renderers = go.GetComponentsInChildren<Renderer>();

            if (renderers == lastRenderers) return false;
            lastRenderers = renderers;

            if (renderers != null && renderers.Length > 0)
            {
                _lastGameObjects = renderers.Select(r => r.gameObject).ToArray();

                foreach (SceneView view in SceneView.sceneViews)
                {
                    SceneViewOutline outline;
                    if (!viewDict.TryGetValue(view, out outline))
                    {
                        outline = view.camera.gameObject.AddComponent<SceneViewOutline>();
                        viewDict.Add(view, outline);
                    }
                    outline.SetRenderer(renderers);
                    view.Repaint();
                }
            }
            else
            {
                _lastGameObjects = null;

                foreach (SceneView view in SceneView.sceneViews)
                {
                    SceneViewOutline outline;
                    if (!viewDict.TryGetValue(view, out outline))
                    {
                        outline = view.camera.gameObject.AddComponent<SceneViewOutline>();
                        viewDict.Add(view, outline);
                    }
                    outline.SetRenderer(null);
                    view.Repaint();
                }
            }

            return true;
        }

        private static void HighlightOnHierarchy(Rect rect, Event e, GameObject go)
        {
            switch (e.type)
            {
                case EventType.MouseDrag:
                case EventType.DragUpdated:
                case EventType.DragPerform:
                case EventType.DragExited:
                case EventType.MouseMove:
                    if (go == null) break;
                    bool contains = rect.Contains(e.mousePosition);
                    if (contains && e.modifiers == EventModifiers.Control)
                    {
                        if (hoveredGO != go)
                        {
                            hoveredGO = go;
                            Highlight(go);
                        }
                    }
                    else if (hoveredGO == go)
                    {
                        hoveredGO = null;
                        Highlight(null);
                    }

                    break;
            }
        }

        public static void RepaintAllHierarchies()
        {
            if (!Prefs.highlightHierarchyRow) return;

            Object[] windows = UnityEngine.Resources.FindObjectsOfTypeAll(EditorTypes.sceneHierarchyWindow);
            foreach (Object w in windows) (w as EditorWindow).Repaint();
        }


        [ExecuteInEditMode]
        public class SceneViewOutline : MonoBehaviour
        {
            private bool useRenderImage;
            private CommandBuffer _buffer;
            private Material _material;
            private Material _childMaterial;
            private Shader _shader;

            private CommandBuffer buffer
            {
                get
                {
                    if (_buffer == null) _buffer = new CommandBuffer();

                    return _buffer;
                }
            }

            private Material material
            {
                get
                {
                    if (_material == null) _material = new Material(shader);
                    return _material;
                }
            }

            private Shader shader
            {
                get
                {
                    if (_shader == null) _shader = Shader.Find("Hidden/InfinityCode/uContext/SceneViewHighlight");
                    return _shader;
                }
            }

            public void OnRenderImage(RenderTexture source, RenderTexture destination)
            {
                if (source == null || destination == null) return;
                CommandBuffer b = buffer;

                if (!useRenderImage)
                {
                    b.Clear();
                    Graphics.Blit(source, destination);
                    return;
                }

                int width = source.width;
                int height = source.height;
                RenderTexture t1 = RenderTexture.GetTemporary(width, height, 32);
                RenderTexture t2 = RenderTexture.GetTemporary(width, height, 32);

                RenderTexture.active = t1;
                GL.Clear(true, true, Color.clear);

                Graphics.ExecuteCommandBuffer(b);
                RenderTexture.active = null;

                Material m = material;

                Graphics.Blit(t1, t2, m, 1);
                Graphics.Blit(t2, t1, m, 2);

                m.SetTexture("_OutlineTex", t1);
                m.SetTexture("_FillTex", t2);
                m.SetColor("_Color", Prefs.highlightColor);
                Graphics.Blit(source, destination, m, 3);

                RenderTexture.ReleaseTemporary(t1);
                RenderTexture.ReleaseTemporary(t2);
            }

            public void SetRenderer(Renderer[] renderers)
            {
                CommandBuffer b = buffer;
                b.Clear();

                useRenderImage = renderers != null && renderers.Length > 0;
                if (!useRenderImage) return;

                Material m = material;

                int id = 1;
                foreach (Renderer r in renderers.OrderBy(r => SortingLayer.GetLayerValueFromID(r.sortingLayerID)).ThenBy(r => r.sortingOrder))
                {
                    int count = 1;
                    if (r is MeshRenderer)
                    {
                        MeshFilter f = r.GetComponent<MeshFilter>();
                        if (f != null && f.sharedMesh != null) count = f.sharedMesh.subMeshCount;
                    }
                    else 
                    {
                        SkinnedMeshRenderer smr = r as SkinnedMeshRenderer;
                        if (smr != null && smr.sharedMesh != null) count = smr.sharedMesh.subMeshCount;
                    }

                    Color32 objectID = new Color32((byte)(id & 0xff), (byte)((id >> 8) & 0xff), 0, 0);
                    b.SetGlobalColor("_ObjectID", objectID);
                    for (int i = 0; i < count; i++) b.DrawRenderer(r, m, i, 0);
                    id++;
                }
            }
        }
    }
}
