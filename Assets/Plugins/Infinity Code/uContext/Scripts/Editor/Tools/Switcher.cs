﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections;
using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Tools
{
    [InitializeOnLoad]
    public static class Switcher
    {
        static Switcher()
        {
            KeyManager.KeyBinding binding = KeyManager.AddBinding();
            binding.OnValidate += () => Prefs.switcher;
            binding.OnInvoke += OnInvoke;

            EditorApplication.playModeStateChanged += EditorApplicationOnPlayModeStateChanged;
        }

        private static void EditorApplicationOnPlayModeStateChanged(PlayModeStateChange mode)
        {
            if (mode == PlayModeStateChange.EnteredPlayMode)
            {
                if (SceneView.lastActiveSceneView != null && SceneView.lastActiveSceneView.maximized)
                {
                    SceneView.lastActiveSceneView.maximized = false;
                    SceneView.lastActiveSceneView.Repaint();

                    IList list = EditorReflected.GetGameViews();
                    if (list != null && list.Count > 0)
                    {
                        EditorWindow gameView = list[0] as EditorWindow;
                        if (gameView != null)
                        {
                            gameView.Focus();
                            gameView.maximized = true;
                        }
                    }
                }
            }
            else if (mode == PlayModeStateChange.EnteredEditMode)
            {
                IList list = EditorReflected.GetGameViews();
                if (list != null && list.Count > 0)
                {
                    EditorWindow gameView = list[0] as EditorWindow;
                    if (gameView != null && gameView.maximized)
                    {
                        gameView.maximized = false;
                        gameView.Repaint();

                        if (SceneView.lastActiveSceneView != null)
                        {
                            SceneView.lastActiveSceneView.Focus();
                            SceneView.lastActiveSceneView.maximized = true;
                        }
                    }
                }
            }
        }

        private static void OnInvoke()
        {
            Event e = Event.current;
            if (Prefs.switcherWindows && e.keyCode == Prefs.switcherWindowsKeyCode && e.modifiers == Prefs.switcherWindowsModifiers) OnSwitch();
        }

        private static void OnSwitch()
        {
            EditorWindow focusedWindow = EditorWindow.focusedWindow;
            bool maximized = focusedWindow != null && focusedWindow.maximized;
            if (maximized)
            {
                focusedWindow.maximized = false;
                focusedWindow.Repaint();
            }
            if (focusedWindow == SceneView.lastActiveSceneView)
            {
                IList list = EditorReflected.GetGameViews();
                if (list != null && list.Count > 0)
                {
                    EditorWindow gameView = list[0] as EditorWindow;
                    if (gameView != null)
                    {
                        gameView.Focus();
                        if (maximized) gameView.maximized = true;
                    }
                }
            }
            else
            {
                if (SceneView.sceneViews == null || SceneView.sceneViews.Count == 0) return;
                EditorWindow sceneView = SceneView.sceneViews[0] as EditorWindow;
                if (sceneView != null)
                {
                    sceneView.Focus();
                    if (maximized) sceneView.maximized = true;
                    if (EditorApplication.isPlaying) EditorApplication.isPaused = true;
                }
            }
        }
    }
}