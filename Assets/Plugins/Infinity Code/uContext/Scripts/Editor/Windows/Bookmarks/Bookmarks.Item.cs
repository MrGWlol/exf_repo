﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InfinityCode.uContext.Windows
{
    public partial class Bookmarks
    {
        public class Item : SearchableItem
        {
            private const string Separator = "|";
            private static GUIStyle audioClipButtonStyle;
            public int instanceID;
            public int temporaryID = int.MaxValue;
            public string title;
            public string type;
            public string path;
            public string tooltip;
            public Object target;
            public bool isMissed = false;
            public bool isProjectItem;
            public Texture preview;
            private string[] _search;

            public GameObject gameObject
            {
                get
                {
                    if (target == null) return null;
                    if (target is Component) return (target as Component).gameObject;
                    if (target is GameObject) return target as GameObject;
                    return null;
                }
            }

            public Item()
            {

            }

            public Item(Object obj)
            {
                target = obj;

                instanceID = obj.GetInstanceID();
                Type t = obj.GetType();
                type = t.AssemblyQualifiedName;

                if (obj is GameObject)
                {
                    GameObject go = obj as GameObject;
                    title = go.name;
                    isProjectItem = go.scene.name == null;
                    tooltip = GameObjectUtils.GetTransformPath(go.transform).ToString();

                    if (!isProjectItem) path = "/" + tooltip;
                    else path = AssetDatabase.GetAssetOrScenePath(go);
                }
                else if (obj is Component)
                {
                    GameObject go = (obj as Component).gameObject;
                    title = go.name + " (" + ObjectNames.NicifyVariableName(t.Name) + ")";
                    tooltip = GameObjectUtils.GetTransformPath(go.transform).ToString();

                    isProjectItem = go.scene.name == null;

                    if (!isProjectItem) path = "/" + tooltip;
                    else path = AssetDatabase.GetAssetPath(go);
                }
                else
                {
                    title = obj.name;
                    tooltip = AssetDatabase.GetAssetPath(instanceID);
                    path = AssetDatabase.GetAssetOrScenePath(obj);
                    isProjectItem = true;
                }
            }

            public void Dispose()
            {
                target = null;
                preview = null;
                _search = null;
            }

            public bool DrawCell(Rect rect)
            {
                bool returnVal = true;

                if (!isMissed && target == null) TryRestoreTarget();

                bool selected = !isMissed && target != null && Selection.activeObject == target;
                EditorGUI.BeginDisabledGroup(isMissed);

                if (preview == null) InitPreview();

                ProcessCellEvents(rect);

                if (selected)
                {
                    GUI.Box(new RectOffset(2, 2, 2, 2).Add(rect), GUIContent.none, selectedStyle);
                }

                GUI.DrawTexture(new Rect(rect.x + gridMargin, rect.y, gridSize, gridSize), preview);
                GUIContent content = new GUIContent(title);
                GUIStyle style = new GUIStyle(EditorStyles.miniLabel);
                Vector2 size = style.CalcSize(content);
                if (size.x < rect.width) style.alignment = TextAnchor.MiddleCenter;
                GUI.Label(new Rect(rect.x, rect.y + gridSize, rect.width, 20), content, style);

                EditorGUI.EndDisabledGroup();

                return returnVal;
            }

            public bool DrawRow()
            {
                bool returnVal = true;

                if (!isMissed && target == null) TryRestoreTarget();

                bool selected = !isMissed && target != null && Selection.activeObject == target;
                EditorGUILayout.BeginHorizontal(selected ? Styles.selectedRow : Styles.transparentRow);

                EditorGUI.BeginDisabledGroup(isMissed);
                DrawRowFirstButton();
                DrawRowSecondButton();

                ButtonEvent event1 = DrawRowPreview();

                GUIContent content = new GUIContent(title, !isMissed? GetShowTooltip(): "(Missed) " + GetShowTooltip());
                ButtonEvent event2 = GUILayoutUtils.Button(content, EditorStyles.label, GUILayout.Height(20), GUILayout.MaxWidth(instance.position.width - 100));

                ProcessRowEvents(event1, event2);

                EditorGUI.EndDisabledGroup();

                if (folderItems == null && GUILayout.Button(closeContent, Styles.transparentButton, GUILayout.ExpandWidth(false), GUILayout.Height(12))) returnVal = false;

                EditorGUILayout.EndHorizontal();

                return returnVal;
            }

            private void DrawRowFirstButton()
            {
                showContent.tooltip = GetShowTooltip();
                if (GUILayout.Button(showContent, showContentStyle, GUILayout.ExpandWidth(false), GUILayout.Height(12)))
                {
                    if (target is Component) ComponentWindow.Show(target as Component);
                    else if (target as GameObject)
                    {
                        if (isProjectItem)
                        {
                            GameObjectUtils.OpenPrefab(path);
                        }
                        else
                        {
                            Selection.activeGameObject = gameObject;
                            WindowsHelper.ShowInspector();
                        }
                    }
                    else EditorUtility.OpenWithDefaultApp(AssetDatabase.GetAssetPath(target));
                }
            }

            private ButtonEvent DrawRowPreview()
            {
                if (preview == null) InitPreview();

                ButtonEvent event1 = GUILayoutUtils.Button(preview, GUIStyle.none, GUILayout.Height(20), GUILayout.Width(20));
                return event1;
            }

            private void DrawRowSecondButton()
            {
                if (isProjectItem)
                {
                    if (target is AudioClip) PlayStopAudioClipButton();
                    else GUILayout.Space(20);
                }
#if UNITY_2019_1_OR_NEWER
                else if (isGameObjectHidden != null && gameObject != null)
                {
                    bool hidden = (bool) isGameObjectHidden.Invoke(null, new object[] {gameObject});
                    if (GUILayout.Button(hidden ? hiddenContent : visibleContent, Styles.transparentButton, GUILayout.ExpandWidth(false)))
                    {
#if UNITY_2019_2_OR_NEWER
                        toggleGameObjectVisibility.Invoke(sceneVisibilityManagerInstanceProp.GetValue(null, null), new object[] {gameObject, true});
#else
                        toggleGameObjectVisibility.Invoke(null, new object[]{gameObject});
#endif
                    }
                }
#endif
                else
                {
                    GUILayout.Space(20);
                }
            }

            private string Escape(string s)
            {
                return s.Replace("%", "%25").Replace(Separator, "%2C");
            }

            protected override string[] GetSearchStrings()
            {
                if (_search == null) _search = new[] { title };

                return _search;
            }

            private string GetShowTooltip()
            {
                string tooltip = "Show";

                if (isProjectItem)
                {
                    if (target is GameObject) tooltip = "Open Prefab";
                    else if (target is Component) tooltip = "Open Component Window";
                    else if (target is DefaultAsset) tooltip = "Show In Explorer";
                    else tooltip = "Open in default application";
                }
                else
                {
                    if (target is GameObject) tooltip = "Open Inspector Window";
                    else if (target is Component) tooltip = "Open Component Window";
                }

                return tooltip;
            }

            private void InitPreview()
            {
                if (isMissed || target == null) preview = emptyTexture;
                else if (isProjectItem && target is GameObject)
                {
#if UNITY_2020_1_OR_NEWER
                    preview = AssetPreview.GetMiniThumbnail(target);
#else
                    preview = AssetPreview.GetAssetPreview(target);
#endif
                }
                else preview = AssetPreview.GetMiniThumbnail(target);
            }

            public static Item Parse(int version, string line)
            {
                try
                {
                    Item item = new Item();
                    string[] parts = line.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);
                    item.instanceID = int.Parse(parts[0]);
                    item.title = Unescape(parts[1]);
                    item.type = parts[2];
                    item.path = Unescape(parts[3]);
                    item.tooltip = item.path;
                    if (!item.path.StartsWith("Assets/")) item.tooltip = item.tooltip.Substring(1);
                    if (version == 2) item.isProjectItem = int.Parse(parts[4]) == 1;
                    return item;
                }
                catch
                {

                }

                return null;
            }

            private void PlayStopAudioClipButton()
            {
                AudioClip audioClip = target as AudioClip;
                bool isPlayed = AudioUtils.IsClipPlaying(audioClip);
                GUIContent playContent = isPlayed ? EditorIconContents.pauseButtonOn : EditorIconContents.playButtonOn;

                if (audioClipButtonStyle == null)
                {
                    audioClipButtonStyle = new GUIStyle();
                    audioClipButtonStyle.margin.top = 3;
                }

                if (GUILayout.Button(playContent, audioClipButtonStyle, GUILayout.Width(20)))
                {
                    if (isPlayed) AudioUtils.StopClip(audioClip);
                    else AudioUtils.PlayClip(audioClip);
                }
            }

            private void ProcessCellEvents(Rect rect)
            {
                Event e = Event.current;
                if (!rect.Contains(e.mousePosition)) return;
                if (e.type == EventType.MouseUp)
                {
                    if (e.button == 0)
                    {
                        if (Selection.activeObject == target)
                        {
                            ProcessDoubleClick();
                        }
                        else
                        {
                            lastClickTime = EditorApplication.timeSinceStartup;
                            Selection.activeObject = target;
                            EditorGUIUtility.PingObject(target);
                        }
                        e.Use();
                    }
                    else if (e.button == 1)
                    {
                        ShowContextMenu();
                        e.Use();
                    }
                }
                else if (e.type == EventType.MouseDrag)
                {
                    if (GUIUtility.hotControl == 0)
                    {
                        DragAndDrop.PrepareStartDrag();
                        DragAndDrop.objectReferences = new[] {target};

                        DragAndDrop.StartDrag("Drag " + target);
                        e.Use();
                    }
                }
            }

            private void ProcessDoubleClick()
            {
                if (EditorApplication.timeSinceStartup - lastClickTime > 0.3)
                {
                    lastClickTime = EditorApplication.timeSinceStartup;
                    return;
                }

                lastClickTime = 0;

                if (target is AudioClip)
                {
                    AudioClip audioClip = target as AudioClip;
                    if (AudioUtils.IsClipPlaying(audioClip)) AudioUtils.StopClip(audioClip);
                    else AudioUtils.PlayClip(audioClip);
                }
                else if (target is DefaultAsset)
                {
                    FileAttributes attributes = File.GetAttributes(path);
                    if ((attributes & FileAttributes.Directory) == FileAttributes.Directory) SelectFolder(this);
                }
                else if (target is Component)
                {
                    ComponentWindow.Show(target as Component);
                }
                else if (target is GameObject)
                {
                    if (PrefabUtility.IsPartOfAnyPrefab(gameObject))
                    {
                        GameObjectUtils.OpenPrefab(path);
                    }
                }
                else EditorUtility.OpenWithDefaultApp(path);
            }

            private void ProcessRowEvents(ButtonEvent event1, ButtonEvent event2)
            {
                Event e = Event.current;
                if (event1 == ButtonEvent.click || event2 == ButtonEvent.click)
                {
                    if (e.button == 0)
                    {
                        if (Selection.activeObject == target)
                        {
                            ProcessDoubleClick();
                        }
                        else
                        {
                            lastClickTime = EditorApplication.timeSinceStartup;
                            Selection.activeObject = target;
                            EditorGUIUtility.PingObject(target);
                        }
                        
                        e.Use();
                    }
                    else if (e.button == 1)
                    {
                        ShowContextMenu();
                        e.Use();
                    }
                }
                else if (event1 == ButtonEvent.drag || event2 == ButtonEvent.drag)
                {
                    DragAndDrop.PrepareStartDrag();
                    DragAndDrop.objectReferences = new[] {target};

                    DragAndDrop.StartDrag("Drag " + target);
                    e.Use();
                }
            }

            private void ShowContextMenu()
            {
                if (target is Component)
                {
                    ComponentUtils.ShowContextMenu(target as Component);
                }
                else if (target is GameObject)
                {
                    if (!isProjectItem) GameObjectUtils.ShowContextMenu(false, target as GameObject);
                    else ShowOtherContextMenu();
                }
                else ShowOtherContextMenu();
            }

            private void ShowOtherContextMenu()
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("Remove Bookmark"), false, () => RemoveLate(this));
                menu.ShowAsContext();
            }

            public void TryRestoreTarget()
            {
                Type t = Type.GetType(type);

                bool isGameObject = t == typeof(GameObject);

                if (isGameObject || t.IsSubclassOf(typeof(Component)))
                {
                    if (instanceID < 0)
                    {
                        target = EditorUtility.InstanceIDToObject(instanceID);
                        if (target != null && t == target.GetType())
                        {
                            temporaryID = int.MaxValue;
                            return;
                        }
                        target = null;
                    }

                    GameObject go;
                    if (instanceID > 0 && path.StartsWith("Assets/")) go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    else go = GameObject.Find(path);

                    if (go == null)
                    {
                        isMissed = true;
                        return;
                    }

                    if (isGameObject)
                    {
                        target = go;
                        temporaryID = go.GetInstanceID();
                        return;
                    }

                    Component component = go.GetComponent(t);

                    if (component != null)
                    {
                        target = component;
                        temporaryID = component.GetInstanceID();
                    }
                    else isMissed = true;
                }
                else
                {
                    target = AssetDatabase.LoadAssetAtPath<Object>(path);
                    if (target == null)
                    {
                        isMissed = true;
                    }
                    else if (target.GetType() != t)
                    {
                        target = null;
                        isMissed = true;
                    }
                    else temporaryID = target.GetInstanceID();
                }
            }

            private static string Unescape(string s)
            {
                return s.Replace("%2C", Separator).Replace("%25", "%");
            }

            public float Update(string pattern, string valueType)
            {
                accuracy = 0;

                if (!string.IsNullOrEmpty(valueType) && !Contains(type, valueType)) return 0;
                return UpdateAccuracy(pattern);
            }

            public void Write(StreamWriter writer)
            {
                StaticStringBuilder.Clear();
                StaticStringBuilder.Append(instanceID).Append(Separator);
                StaticStringBuilder.Append(Escape(title)).Append(Separator);
                StaticStringBuilder.Append(Escape(type)).Append(Separator);
                StaticStringBuilder.Append(Escape(path)).Append(Separator);
                StaticStringBuilder.Append(isProjectItem ? 1 : 0);
                writer.WriteLine(StaticStringBuilder.GetString());
            }
        }
    }
}