﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InfinityCode.uContext.Windows
{
    [InitializeOnLoad]
    public partial class Bookmarks : EditorWindow
    {
        #region Constants

        private const string Filename = "uContextBookmarks.bmk";
        private const string FileVersion = "2";
        private const string GridSizePref = Prefs.Prefix + "Bookmarks.GridSize";
        private const int gridMargin = 10;

        #endregion

        #region Fields

#if UNITY_2019_2_OR_NEWER
        private static PropertyInfo sceneVisibilityManagerInstanceProp;
#endif
#if UNITY_2019_1_OR_NEWER
        private static MethodInfo isGameObjectHidden;
        private static GUIContent hiddenContent;
        private static GUIContent visibleContent;
        private static MethodInfo toggleGameObjectVisibility;
#endif

        public static Action<Bookmarks> OnToolbarMiddle;

        private static HashSet<Item> _items;
        private static Item[] sceneItems;
        private static Item[] projectItems;
        private static Item[] filteredItems;
        private static Item[] folderItems;
        private static List<Item> folderItemsStack;
        private static Item removeLateItem;

        private static Bookmarks instance;

        private static GUIContent closeContent;
        private static GUIContent showContent;
        private static GUIStyle showContentStyle;
        private static GUIStyle selectedStyle;
        private static Texture2D emptyTexture;
        private static int gridSize = 47;
        private static double lastClickTime;

        private bool focusOnSearch;
        private Vector2 scrollPosition;
        private bool showProjectItems = true;
        private bool showSceneItems = true;
        private int minGridSize = 47;
        private int maxGridSize = 128;
        private string _filter = "";

        #endregion

        public static HashSet<Item> items
        {
            get
            {
                if (_items == null) Load();
                return _items;
            }
        }

        public string filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                UpdateFilteredItems();
            }
        }

        static Bookmarks()
        {
            KeyManager.KeyBinding binding = KeyManager.AddBinding();
            binding.OnValidate += OnValidate;
            binding.OnInvoke += () => ShowWindow();

#if UNITY_2019_1_OR_NEWER
            isGameObjectHidden = EditorTypes.sceneVisibilityState.GetMethod("IsGameObjectHidden", BindingFlags.Static | BindingFlags.Public, null, new[] { typeof(GameObject) }, null);
#endif

            Type sceneVisibilityManagerType = EditorTypes.sceneVisibilityManager;

#if UNITY_2019_2_OR_NEWER
            toggleGameObjectVisibility = sceneVisibilityManagerType.GetMethod("ToggleVisibility", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, new[] { typeof(GameObject), typeof(bool) }, null);
            Type ssType = typeof(ScriptableSingleton<>);
            Type[] typeArgs = { sceneVisibilityManagerType };
            sceneVisibilityManagerType = ssType.MakeGenericType(typeArgs);
            sceneVisibilityManagerInstanceProp = sceneVisibilityManagerType.GetProperty("instance", BindingFlags.Static | BindingFlags.Public);
#elif UNITY_2019_1_OR_NEWER
            toggleGameObjectVisibility = sceneVisibilityManagerType.GetMethod("ToggleGameObjectVisibility", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public, null, new[] { typeof(GameObject) }, null);
#endif
        }

        public static void Add(Object target)
        {
            int id = target.GetInstanceID();
            if (items.Any(i => i.instanceID == id || i.temporaryID == id)) return;

            Item item = new Item(target);
            items.Add(item);
            Save();
        }

        private void BottomBar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            EditorGUILayout.Space();
            EditorGUI.BeginChangeCheck();
            gridSize = (int)GUILayout.HorizontalSlider(gridSize, minGridSize, maxGridSize, GUILayout.Width(100));
            if (EditorGUI.EndChangeCheck()) EditorPrefs.SetInt(GridSizePref, gridSize);
            EditorGUILayout.EndHorizontal();
        }

        private static void ClearFilter()
        {
            instance._filter = "";
            filteredItems = null;

            var recycledEditorField = typeof(EditorGUI).GetField("s_RecycledEditor", BindingFlags.Static | BindingFlags.NonPublic);
            object recycledEditor = recycledEditorField.GetValue(null);
            var textProp = typeof(TextEditor).GetProperty("text", BindingFlags.Instance | BindingFlags.Public);
            textProp.SetValue(recycledEditor, string.Empty, null);
        }

        public static bool Contain(Object item)
        {
            int id = item.GetInstanceID();
            return items.Any(i => i.instanceID == id || i.temporaryID == id);
        }

        private static void DisposeFolderItems()
        {
            if (folderItems == null) return;

            foreach (Item item in folderItems) item.Dispose();
            folderItems = null;
        }

        private void DrawItems(ref Item removeItem)
        {
            if (filteredItems != null)
            {
                if (gridSize > minGridSize) DrawGridItems(filteredItems, ref removeItem);
                else DrawTreeItems(filteredItems, ref removeItem);
            }
            else if (folderItems != null)
            {
                if (gridSize > minGridSize) DrawGridItems(folderItems, ref removeItem);
                else DrawTreeItems(folderItems, ref removeItem);
            }
            else
            {
                if (sceneItems == null || projectItems == null) Load();

                if (sceneItems.Length > 0)
                {
#if UNITY_2019_1_OR_NEWER
                    showSceneItems = GUILayout.Toggle(showSceneItems, "Scene Items", EditorStyles.foldoutHeader);
#else
                    showSceneItems = GUILayout.Toggle(showSceneItems, "Scene Items", EditorStyles.foldout);
#endif
                    if (showSceneItems)
                    {
                        if (gridSize > minGridSize) DrawGridItems(sceneItems, ref removeItem);
                        else DrawTreeItems(sceneItems, ref removeItem);
                    }
                }

                if (projectItems.Length > 0)
                {
#if UNITY_2019_1_OR_NEWER
                    showProjectItems = GUILayout.Toggle(showProjectItems, "Project Items", EditorStyles.foldoutHeader);
#else
                    showProjectItems = GUILayout.Toggle(showProjectItems, "Project Items", EditorStyles.foldout);
#endif
                    if (showProjectItems)
                    {
                        if (gridSize > minGridSize) DrawGridItems(projectItems, ref removeItem);
                        else DrawTreeItems(projectItems, ref removeItem);
                    }
                }
            }
        }

        private void DrawGridItems(Item[] items, ref Item removeItem)
        {
            int countCols = Mathf.FloorToInt((position.width - 30) / (gridSize + gridMargin * 2));
            int countRows = Mathf.CeilToInt( items.Length / (float)countCols);
            int rowHeight = gridSize + 20;
            int width = Mathf.Min(countCols, items.Length) * (gridSize + gridMargin * 2);
            int height = countRows * (rowHeight);

            float marginLeft = (position.width - width) / 2;

            GUILayout.Box(GUIContent.none, GUIStyle.none, GUILayout.Width(width), GUILayout.Height(height));
            Rect rect = GUILayoutUtility.GetLastRect();

            for (int i = 0; i < items.Length; i++)
            {
                int row = i / countCols;
                int col = i % countCols;
                Rect r = new Rect(col * (gridSize + gridMargin * 2) + marginLeft, row * rowHeight + rect.y, gridSize + gridMargin * 2, rowHeight);
                Item item = items[i];
                if (!item.DrawCell(r)) removeItem = item;
            }
        }

        private static void DrawTreeItems(Item[] items, ref Item removeItem)
        {
            foreach (Item item in items)
            {
                if (!item.DrawRow()) removeItem = item;
            }
        }

        private void FolderItemsToolbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            Item current = folderItemsStack.Last();

            if (folderItemsStack.Count > 1)
            {
                if (GUILayout.Button(EditorIconContents.animationFirstKey, EditorStyles.toolbarButton, GUILayout.ExpandWidth(false)))
                {
                    ClearFilter();

                    DisposeFolderItems();
                    folderItemsStack.Clear();
                    folderItems = null;
                    return;
                }
            }

            if (GUILayout.Button(EditorIconContents.animationPrevKey, EditorStyles.toolbarButton, GUILayout.ExpandWidth(false)))
            {
                ClearFilter();

                folderItemsStack.RemoveAt(folderItemsStack.Count - 1);
                if (folderItemsStack.Count > 0) SelectParentFolder(folderItemsStack.Last());
                else DisposeFolderItems();
            }

            EditorGUILayout.LabelField(current.title);
            EditorGUILayout.EndHorizontal();
        }

        private static void InitFolderItems(Item folderItem)
        {
            DisposeFolderItems();

            IEnumerable<string> entries = Directory.GetFileSystemEntries(folderItem.path);
            List<Item> tempItems = new List<Item>();
            foreach (string entry in entries)
            {
                if (entry.EndsWith(".meta")) continue;

                Object asset = AssetDatabase.LoadAssetAtPath<Object>(entry);
                if (asset == null) continue;

                Item item = new Item(asset);
                tempItems.Add(item);
            }

            folderItems = tempItems.ToArray();
        }

        public static void InsertBookmarkMenu(GenericMenu menu, Object target)
        {
            if (Contain(target)) menu.AddItem(new GUIContent("Remove Bookmark"), false, () => Remove(target));
            else menu.AddItem(new GUIContent("Add Bookmark"), false, () => Add(target));
        }

        public static void InsertBookmarkMenu(GenericMenuEx menu, Object target)
        {
            if (Contain(target)) menu.Add("Remove Bookmark", () => Remove(target));
            else menu.Add("Add Bookmark", () => Add(target));
        }

        private static void Load()
        {
            _items = new HashSet<Item>();
            if (!File.Exists(Filename))
            {
                sceneItems = new Item[0];
                projectItems = new Item[0];
                return;
            }
            FileStream stream = File.OpenRead(Filename);
            StreamReader reader = new StreamReader(stream);

            if (reader.EndOfStream)
            {
                reader.Close();
                return;
            }
            string versionStr = reader.ReadLine();
            int version;
            if (string.IsNullOrEmpty(versionStr) || !int.TryParse(versionStr, out version))
            {
                version = 1;
                Debug.Log("No version");
            }

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                Item item = Item.Parse(version, line);
                if (item != null) _items.Add(item);
            }

            reader.Close();

            UpdateItemsGroup();
        }

        private void OnDestroy()
        {
            if (folderItems != null)
            {
                foreach (Item item in folderItems) item.Dispose();
                folderItems = null;
            }

            folderItemsStack = null;

            instance = null;
        }

        private void OnEnable()
        {
            instance = this;
            minSize = new Vector2(250, 250);

            gridSize = EditorPrefs.GetInt(GridSizePref, 16);

            showContent = new GUIContent(Styles.isProSkin? Icons.openNewWhite: Icons.openNewBlack, "Show");
            closeContent = new GUIContent(Styles.isProSkin ? Icons.closeWhite: Icons.closeBlack, "Remove");

#if UNITY_2019_1_OR_NEWER
            hiddenContent = EditorIconContents.sceneVisibilityHiddenHover;
            visibleContent = EditorIconContents.sceneVisibilityVisibleHover;
#endif
        }

        private void OnGUI()
        {
            if (instance == null) instance = this;
            if (emptyTexture == null) emptyTexture = Resources.CreateSinglePixelTexture(1f, 1f, 1f, 0f);
            if (showContentStyle == null || showContentStyle.normal.background == null)
            {
                showContentStyle = new GUIStyle(Styles.transparentButton);
                showContentStyle.margin.top = 6;
                showContentStyle.margin.left = 6;
            }
            if (selectedStyle == null)
            {
                selectedStyle = new GUIStyle(Styles.selectedRow);
                selectedStyle.fixedHeight = 0;
            }

            if (removeLateItem != null)
            {
                Remove(removeLateItem.target);
                removeLateItem = null;
            }

            ProcessEvents();
            Toolbar();

            if (folderItemsStack != null && folderItemsStack.Count > 0)
            {
                FolderItemsToolbar();
            }

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            Item removeItem = null;
            DrawItems(ref removeItem);

            if (removeItem != null)
            {
                _items.Remove(removeItem);
                Save();
                UpdateFilteredItems();
            }

            EditorGUILayout.EndScrollView();

            BottomBar();
        }

        private static bool OnValidate()
        {
            return Prefs.bookmarksHotKey && Event.current.modifiers == Prefs.bookmarksModifiers && Event.current.keyCode == Prefs.bookmarksKeyCode;
        }

        private void ProcessEvents()
        {
            if (mouseOverWindow != this) return;
            if (folderItems != null) return;

            Event e = Event.current;
            if (e.type == EventType.DragUpdated)
            {
                DragAndDrop.visualMode = DragAndDropVisualMode.Link;
                e.Use();
            }
            else if (e.type == EventType.DragPerform)
            {
                DragAndDrop.AcceptDrag();
                e.Use();

                foreach (Object obj in DragAndDrop.objectReferences) Add(obj);
            }
            else if (e.type == EventType.KeyDown)
            {
                if (e.keyCode == KeyCode.F && (e.modifiers == EventModifiers.Control || e.modifiers == EventModifiers.Command))
                {
                    focusOnSearch = true;
                    e.Use();
                }
            }
        }

        public static void Redraw()
        {
            if (instance != null) instance.Repaint();
        }

        private static void RefreshItems()
        {
            foreach (Item item in items) item.isMissed = false;
        }

        public static void Remove(Object item)
        {
            int id = item.GetInstanceID();
            items.RemoveWhere(i => i.instanceID == id || i.temporaryID == id);
            Save();
            if (instance != null) instance.UpdateFilteredItems();
        }

        private static void RemoveLate(Item item)
        {
            removeLateItem = item;
        }

        public static void Save()
        {
            if (File.Exists(Filename)) File.Delete(Filename);

            FileStream stream = File.OpenWrite(Filename);
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(FileVersion);

            foreach (Item item in items) item.Write(writer);

            writer.Close();

            UpdateItemsGroup();
        }

        private static void SelectFolder(Item folderItem)
        {
            ClearFilter();

            InitFolderItems(folderItem);

            if (folderItemsStack == null) folderItemsStack = new List<Item>();
            folderItemsStack.Add(folderItem);
        }

        private void SelectParentFolder(Item folderItem)
        {
            InitFolderItems(folderItem);
        }

        public static EditorWindow ShowDropDownWindow(Vector2? mousePosition = null)
        {
            if (!mousePosition.HasValue) mousePosition = Event.current.mousePosition;
            Bookmarks wnd = CreateInstance<Bookmarks>();
            wnd.titleContent = new GUIContent("Bookmarks");
            Vector2 position = GUIUtility.GUIToScreenPoint(mousePosition.Value);
            Vector2 size = Prefs.contextMenuWindowSize;
            Rect rect = new Rect(position - size / 2, size);
            if (rect.y < 30) rect.y = 30;

            wnd.position = rect;
            wnd.ShowPopup();
            wnd.Focus();

            PinAndClose.Show(wnd, rect, wnd.Close, () =>
            {
                ShowWindow().position = wnd.position;
                wnd.Close();
            });
            return wnd;
        }

        public static EditorWindow ShowWindow(Vector2? mousePosition)
        {
            Bookmarks wnd = CreateInstance<Bookmarks>();
            wnd.titleContent = new GUIContent("Bookmarks");
            wnd.Show();

            Vector2? mp = null;
            if (mousePosition.HasValue) mp = mousePosition.Value;
            else if (Event.current != null) mp = Event.current.mousePosition;

            if (mp.HasValue)
            {
                Vector2 screenPoint = GUIUtility.GUIToScreenPoint(mp.Value);
                Vector2 size = Prefs.contextMenuWindowSize;
                wnd.position = new Rect(screenPoint - size / 2, size);
            }
            return wnd;
        }

        [MenuItem(WindowsHelper.MenuPath + "Bookmarks", false, 100)]
        public static EditorWindow ShowWindow()
        {
            return ShowWindow(null);
        }

        public static EditorWindow ShowUtilityWindow(Vector2? mousePosition = null)
        {
            if (!mousePosition.HasValue) mousePosition = Event.current.mousePosition;
            Bookmarks wnd = CreateInstance<Bookmarks>();
            wnd.titleContent = new GUIContent("Bookmarks");
            wnd.ShowUtility();
            wnd.Focus();
            Vector2 size = Prefs.contextMenuWindowSize;
            wnd.position = new Rect(GUIUtility.GUIToScreenPoint(mousePosition.Value) - size / 2, size);
            return wnd;
        }

        private void Toolbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            EditorGUI.BeginChangeCheck();
            GUI.SetNextControlName("uContextBookmarkSearchTextField");
            _filter = EditorReflected.ToolbarSearchField(_filter);

            if (focusOnSearch && Event.current.type == EventType.Repaint)
            {
                GUI.FocusControl("uContextBookmarkSearchTextField");
                focusOnSearch = false;
            }

            if (EditorGUI.EndChangeCheck()) UpdateFilteredItems();

            if (OnToolbarMiddle != null) OnToolbarMiddle(this);

            if (GUILayout.Button("Refresh", EditorStyles.toolbarButton, GUILayout.ExpandWidth(false))) RefreshItems();
            if (GUILayout.Button("Clear", EditorStyles.toolbarButton, GUILayout.ExpandWidth(false)))
            {
                if (EditorUtility.DisplayDialog("Clear Bookmarks", "Do you really want to clear your bookmarks?", "Clear", "Cancel"))
                {
                    items.Clear();
                    Save();
                    _filter = string.Empty;
                    UpdateFilteredItems();
                }
            }

            EditorGUILayout.EndHorizontal();
        }

        public void UpdateFilteredItems()
        {
            if (string.IsNullOrEmpty(_filter))
            {
                filteredItems = null;
                return;
            }

            string assetType;
            string pattern = SearchableItem.GetPattern(_filter, out assetType);

            IEnumerable<Item> targetItems;
            if (folderItems == null) targetItems = items;
            else targetItems = folderItems;

            filteredItems = targetItems.Where(i => i.Update(pattern, assetType) > 0).OrderByDescending(i => i.accuracy).ToArray();
        }

        private static void UpdateItemsGroup()
        {
            sceneItems = _items.Where(i => !i.isProjectItem).OrderBy(i => i.title).ToArray();
            projectItems = _items.Where(i => i.isProjectItem).OrderBy(i => i.title).ToArray();
        }
    }
}