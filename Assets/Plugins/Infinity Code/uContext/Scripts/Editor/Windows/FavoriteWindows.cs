﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using InfinityCode.uContext.JSON;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace InfinityCode.uContext.Windows
{
    public class FavoriteWindows : EditorWindow
    {
        private const string Filename = "uContextFavoriteWindows.json";

        private ReorderableList reorderableList;
        private static List<FavoriteWindowRecord> _records;
        private bool waitWindowChanged;
        private static bool needSave;

        public static List<FavoriteWindowRecord> records
        {
            get
            {
                if (_records == null) Load();
                return _records;
            }
        }

        private void AddItem(ReorderableList list)
        {
            waitWindowChanged = true;
            EditorApplication.update -= WaitWindowChanged;
            EditorApplication.update += WaitWindowChanged;
        }

        private void DrawElement(Rect rect, int index, bool isactive, bool isfocused)
        {
            FavoriteWindowRecord record = records[index];

            EditorGUI.BeginChangeCheck();
            Rect r = new Rect(rect);
            r.height /= 2;
            record.title = EditorGUI.TextField(r, "Title", record.title);
            if (EditorGUI.EndChangeCheck()) needSave = true;

            EditorGUI.BeginDisabledGroup(true);
            r.y += r.height;
            EditorGUI.TextField(r, "Class", record.className);
            EditorGUI.EndDisabledGroup();
        }

        private void DrawHeader(Rect rect)
        {
            GUI.Label(rect, "Windows");
        }

        private static void Load()
        {
            _records = new List<FavoriteWindowRecord>();

            if (!File.Exists(Filename)) return;
            string text = File.ReadAllText(Filename, Encoding.UTF8);
            JsonArray items = JsonArray.ParseArray(text);
            foreach (JsonItem item in items) _records.Add(new FavoriteWindowRecord(item));
        }

        private void OnDestroy()
        {
            waitWindowChanged = false;
            EditorApplication.update -= WaitWindowChanged;

            if (reorderableList != null)
            {
                reorderableList.drawHeaderCallback -= DrawHeader;
                reorderableList.drawElementCallback -= DrawElement;
                reorderableList.onAddCallback -= AddItem;
                reorderableList.onRemoveCallback -= RemoveItem;
                reorderableList.onReorderCallback -= Reorder;
                reorderableList = null;
            }
        }

        private void OnGUI()
        {
            if (reorderableList == null)
            {
                reorderableList = new ReorderableList(records, typeof(FavoriteWindowRecord), true, true, true, true);
                reorderableList.drawHeaderCallback += DrawHeader;
                reorderableList.drawElementCallback += DrawElement;
                reorderableList.onAddCallback += AddItem;
                reorderableList.onRemoveCallback += RemoveItem;
                reorderableList.onReorderCallback += Reorder;
                reorderableList.elementHeight = 48;
            }

            reorderableList.DoLayoutList();

            if (needSave) Save();

            if (waitWindowChanged)
            {
                EditorGUILayout.HelpBox("Set the focus on the window you want to add to the favorites.", MessageType.Info);

                if (GUILayout.Button("Stop Pick"))
                {
                    waitWindowChanged = false;
                    EditorApplication.update -= WaitWindowChanged;
                }
            }
        }

        [MenuItem(WindowsHelper.MenuPath + "Edit Favorite Windows", false, 100)]
        public static void OpenWindow()
        {
            GetWindow<FavoriteWindows>(false, "Favorite Windows", true);
        }

        private void RemoveItem(ReorderableList list)
        {
            records.RemoveAt(list.index);
            needSave = true;
        }

        private void Reorder(ReorderableList list)
        {
            needSave = true;
        }

        private static void Save()
        {
            JsonArray items = new JsonArray();
            foreach (FavoriteWindowRecord record in records)
            {
                JsonObject item = items.CreateObject();
                item.Add("title", record.title);
                item.Add("class", record.className);
            }

            File.WriteAllText(Filename, items.ToString(), Encoding.UTF8);
            needSave = false;
        }

        private void WaitWindowChanged()
        {
            EditorWindow wnd = focusedWindow;

            if (wnd == null) return;
            if (wnd is FavoriteWindows) return;

            EditorApplication.update -= WaitWindowChanged;
            string className = wnd.GetType().AssemblyQualifiedName;
            if (records.All(r => r.className != className)) records.Add(new FavoriteWindowRecord(wnd));
            Save();
            waitWindowChanged = false;
            Repaint();
        }

        public class FavoriteWindowRecord
        {
            public string className;
            public string title;

            public FavoriteWindowRecord(EditorWindow window)
            {
                Type type = window.GetType();
                className = type.AssemblyQualifiedName;
                title = window.titleContent.text;
            }

            public FavoriteWindowRecord(JsonItem item)
            {
                className = item.V<string>("class");
                title = item.V<string>("title");
            }

            public void Open()
            {
                Type type = Type.GetType(className);
                GetWindow(type, false, title);
            }
        }
    }
}