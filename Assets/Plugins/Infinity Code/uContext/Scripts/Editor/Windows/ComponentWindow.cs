﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

#if UNITY_2019_1_OR_NEWER
using UnityEditor.Compilation;
#endif 

namespace InfinityCode.uContext.Windows
{
    [Serializable]
    public class ComponentWindow : EditorWindow
    {
        public static Action<ComponentWindow> OnDestroyWindow;
        public static Predicate<ComponentWindow> OnDrawContent;
        public static Action<ComponentWindow, Rect, float> OnDrawHeader;
        public static Predicate<ComponentWindow> OnValidateEditor;

        #region Fields

        private static ComponentWindow autoPopupWindow;
        private static GUIStyle inspectorBigStyle;

        [SerializeField]
        public int componentID = -1;

        [SerializeField]
        public string path;

        [SerializeField]
        public string type;

        // 0 - Ignore
        // 1 - Top
        // 2 - Center
        // 3 - Bottom
        [NonSerialized]
        public int adjustHeight = 0;

        [NonSerialized]
        private Component _component;
        private bool isMissedComponent;

        [NonSerialized]
        private Editor editor;
        private GUIStyle linkStyle;
        public Vector2 scrollPosition;
        private GUIContent selectContent;
        private GUIContent bookmarkContent;
        private GUIContent removeBookmarkContent;
        private bool ignoreNextRepaint;
        private float? bottom;
        private bool isAutoPopup;
        private bool isPopup;
        private bool delayedCloseStarted;
        private long delayedCloseStartTime;
        private EventManager.EventBinding binding;
        public bool allowInitEditor = true;
        private GUIContent titleSettingsIcon;
        private MethodInfo drawEditorHeaderItems;
        
        #endregion

        public Component component
        {
            get { return _component; }
            set
            {
                _component = value;
                if (_component != null)
                {
                    componentID = _component.GetInstanceID();

                    if (component.transform != null)
                    {
                        Transform transform = component.transform;
                        path = GameObjectUtils.GetTransformPath(transform).Insert(0, '/').ToString();
                    }

                    type = component.GetType().AssemblyQualifiedName;

                    if (editor != null) DestroyImmediate(editor);
                    InitEditor(_component);
                }
            }
        }

        private bool AdjustHeight(Event e)
        {
            if (bottom.HasValue && e.type == EventType.Repaint)
            {
                if (bottom.Value > 700)
                {
                    adjustHeight = 0;
                    Repaint();
                    return true;
                }

                if (Mathf.Abs(bottom.Value - position.height) > 1)
                {
                    Rect pos = position;

                    if (pos.y < 40) pos.y = 40;

                    pos.height = bottom.Value;
                    position = pos;
                    adjustHeight = 0;
                    Repaint();
                    return true;
                }
            }

            return false;
        }

        private void DrawHeader()
        {
            if (_component == null) return;

            if (inspectorBigStyle == null) inspectorBigStyle = Reflection.GetStaticPropertyValue<GUIStyle>(typeof(EditorStyles), "inspectorBig");// GUI.skin.FindStyle("InspectorBig");

            Event e = Event.current;

            titleSettingsIcon = EditorIconContents.popup;

            GUILayout.BeginHorizontal(inspectorBigStyle);
            GUILayout.Space(38f);
            GUILayout.BeginVertical();
            GUILayout.Space(19f);
            GUILayout.BeginHorizontal();
            EditorGUILayout.GetControlRect();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            Rect lastRect = GUILayoutUtility.GetLastRect();
            Rect r = new Rect(lastRect.x, lastRect.y, lastRect.width, lastRect.height);
            Rect r1 = new Rect(r.x + 6, r.y + 6, 32, 32);

            bool isLoadingAssetPreview = AssetPreview.IsLoadingAssetPreview(componentID);
            Texture2D texture2D = AssetPreview.GetAssetPreview(component);
            if (texture2D == null)
            {
                if (isLoadingAssetPreview) Repaint();
                texture2D = AssetPreview.GetMiniThumbnail(component);
            }

            GUI.Label(r1, texture2D, Styles.centeredLabel);

            Behaviour behaviour = _component as Behaviour;
            Renderer renderer = _component as Renderer;
            if (behaviour != null || renderer != null)
            {
                Rect tr1 = new Rect(r.x + 44, r.y + 5, 16, 18);
                EditorGUI.BeginChangeCheck();
                bool v1 = GUI.Toggle(tr1, behaviour != null ? behaviour.enabled : renderer.enabled, GUIContent.none);
                if (EditorGUI.EndChangeCheck())
                {
                    if (behaviour != null) behaviour.enabled = v1;
                    else renderer.enabled = v1;
                }
            }

            Rect r2 = new Rect(r.x + 60, r.y + 4, r.width - 100, 18);

            GUI.Label(r2, _component.GetType().Name, EditorStyles.largeLabel);
            if (e.type == EventType.MouseDown && r2.Contains(e.mousePosition))
            {
                if (e.button == 1)
                {
                    ComponentUtils.ShowContextMenu(_component);
                    e.Use();
                }
            }
            else if (e.type == EventType.MouseDrag && r2.Contains(e.mousePosition))
            {
                DragAndDrop.PrepareStartDrag();
                DragAndDrop.objectReferences = new[] { _component };

                DragAndDrop.StartDrag("Drag " + _component.name);
                e.Use();
            }

            Vector2 settingsSize = Styles.iconButton.CalcSize(titleSettingsIcon);
            Rect settingsRect = new Rect(r.xMax - settingsSize.x, r.y + 5, settingsSize.x, settingsSize.y);
            if (EditorGUI.DropdownButton(settingsRect, titleSettingsIcon, FocusType.Passive, Styles.iconButton))
            {
                ComponentUtils.ShowContextMenu(_component);
            }
            float offset = settingsSize.x * 2;

#if UNITY_2019_3_OR_NEWER
            drawEditorHeaderItems.Invoke(null, new object[] {new Rect(r.xMax - offset, r.y + 5f, settingsSize.x, settingsSize.y), new UnityEngine.Object[]{component}, 0});
#else
            drawEditorHeaderItems.Invoke(null, new object[] {new Rect(r.xMax - offset, r.y + 5f, settingsSize.x, settingsSize.y), new UnityEngine.Object[]{component}});
#endif

            Rect tr2 = new Rect(r.x + 44, r.y + 25, 16, 18);
            EditorGUI.BeginChangeCheck();
            bool v2 = GUI.Toggle(tr2, _component.gameObject.activeSelf, GUIContent.none);
            if (EditorGUI.EndChangeCheck()) _component.gameObject.SetActive(v2);

            r = new Rect(r.x + 60, r.y + 25, r.width - 100, 18);
            GUI.Label(r, _component.gameObject.name, EditorStyles.boldLabel);
            if (e.type == EventType.MouseDown && r.Contains(e.mousePosition))
            {
                if (e.button == 0)
                {
                    Selection.activeGameObject = _component.gameObject;
                    EditorGUIUtility.PingObject(Selection.activeGameObject);
                    e.Use();
                }
                else if (e.button == 1)
                {
                    GameObjectUtils.ShowContextMenu(false, _component.gameObject);
                    e.Use();
                }
            }
            else if (e.type == EventType.MouseDrag && r.Contains(e.mousePosition))
            {
                DragAndDrop.PrepareStartDrag();
                DragAndDrop.objectReferences = new[] { _component.gameObject };

                DragAndDrop.StartDrag("Drag " + _component.gameObject.name);
                e.Use();
            }

            r.y += 2;

            bool containBookmark = Bookmarks.Contain(component);
            if (GUI.Button(new Rect(position.width - 18, r.y, 16, 16), containBookmark? removeBookmarkContent: bookmarkContent, Styles.transparentButton))
            {
                if (e.modifiers == EventModifiers.Control)
                {
                    Bookmarks.ShowWindow();
                }
                else
                {
                    if (containBookmark) Bookmarks.Remove(component);
                    else Bookmarks.Add(component);
                    Bookmarks.Redraw();
                }
            }

            if (OnDrawHeader != null) OnDrawHeader(this, position, r.y);
        }

        public void FreeEditor()
        {
            if (editor == null) return;

            DestroyImmediate(editor);
            editor = null;
        }

        private void FreeReferences()
        {
            _component = null;
            FreeEditor();
        }

        private bool InitComponent(Event e)
        {
            FreeEditor();

            if (isMissedComponent)
            {
                if (e.type == EventType.Repaint && ignoreNextRepaint)
                {
                    ignoreNextRepaint = false;
                    return false;
                }

                EditorGUILayout.LabelField("Component is missed.");
                if (GUILayout.Button("Try to restore"))
                {
                    TryRestoreComponent();
                    if (isMissedComponent) return false;
                }
            }
            else TryRestoreComponent();

            return true;
        }

        public void InitEditor(Component component)
        {
            FreeEditor();

            if (component is Terrain)
            {
                editor = Reflection.GetStaticFieldValue<Editor>(EditorTypes.terrainInspector, "s_activeTerrainInspectorInstance");
            }
            else
            {
                editor = Editor.CreateEditor(component);
            }
        }

        private void OnCompilationStarted(object obj)
        {
            FreeEditor();
        }

        private void OnDestroy()
        {
            if (OnDestroyWindow != null) OnDestroyWindow(this);

            FreeReferences();

            if (binding != null) binding.Remove();
            if (autoPopupWindow == this) autoPopupWindow = null;

            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        private void OnEnable()
        {
            FreeReferences();
            if (_component == null) _component = EditorUtility.InstanceIDToObject(componentID) as Component;
            if (_component != null)
            {
                if (_component.GetType().AssemblyQualifiedName != type) _component = null;
            }

            selectContent = EditorIconContents.rectTransformBlueprint;
            selectContent.tooltip = "Select GameObject";

            bookmarkContent = new GUIContent(Styles.isProSkin ? Icons.starWhite: Icons.starBlack, "Bookmark");
            removeBookmarkContent = new GUIContent(Icons.starYellow, "Remove Bookmark");

#if UNITY_2019_3_OR_NEWER
            drawEditorHeaderItems = Reflection.GetMethod(typeof(EditorGUIUtility), "DrawEditorHeaderItems", new[] { typeof(Rect), typeof(UnityEngine.Object[]), typeof(float) });
#else
            drawEditorHeaderItems = Reflection.GetMethod(typeof(EditorGUIUtility), "DrawEditorHeaderItems", new[] { typeof(Rect), typeof(UnityEngine.Object[])});
#endif

            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;

#if UNITY_2019_1_OR_NEWER
            CompilationPipeline.compilationStarted += OnCompilationStarted;
#endif
        }

        public void OnGUI()
        {
            Event e = Event.current;
            if (adjustHeight > 0 && AdjustHeight(e)) return;

            if (EditorApplication.isCompiling)
            {
                FreeEditor();
                return;
            }

            if (_component == null && !InitComponent(e)) return;

            if (isMissedComponent) return;

            if (editor == null && allowInitEditor)
            {
                if (_component is Terrain) TryRestoreTerrainEditor();
                else InitEditor(_component);
            }

            if (editor == null)
            {
                if (OnValidateEditor == null || !OnValidateEditor(this)) return;
            }

            if (isPopup)
            {
                GUIStyle style = GUI.skin.box;
                style.normal.textColor = Color.blue;
                GUI.Box(new Rect(0, 0, position.width, position.height), GUIContent.none, style);
            }

            if (isAutoPopup) GUILayout.BeginArea(new Rect(2, 0, position.width - 4, position.height));

            DrawHeader();

            if (OnDrawContent != null && OnDrawContent(this))
            {
                Repaint();
                return;
            }

            if (adjustHeight == 0) scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            editor.OnInspectorGUI();

            if (adjustHeight > 0)
            {
                float b = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(0)).yMin;
                if (e.type == EventType.Repaint) bottom = b + 5; 
            }
            if (adjustHeight == 0) EditorGUILayout.EndScrollView();

            if (isAutoPopup)
            {
                GUILayout.EndArea();

                if (focusedWindow != this && mouseOverWindow != this) StartDelayedClose();
                else if (delayedCloseStarted) StopDelayedClose();
            }

            Repaint();
        }

        private void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode || state == PlayModeStateChange.ExitingPlayMode) allowInitEditor = false;
            else
            {
                allowInitEditor = true;
                Repaint();
            }

            FreeEditor();
        }

        public static ComponentWindow Show(Component component)
        {
            if (component == null) return null;

            ComponentWindow wnd = CreateInstance<ComponentWindow>();

            Texture2D texture2D = AssetPreview.GetAssetPreview(component);
            if (texture2D == null) texture2D = AssetPreview.GetMiniThumbnail(component);

            wnd.titleContent = new GUIContent(component.GetType().Name + " (" + component.gameObject.name + ")", texture2D);
            wnd.component = component;
            wnd.adjustHeight = 2;
            wnd.Show();
            wnd.Focus();
            if (Event.current != null)
            {
                Vector2 screenPoint = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
                Vector2 size = Prefs.contextMenuWindowSize;
                wnd.position = new Rect(screenPoint - size / 2, size);
            }
            return wnd;
        }

        public static ComponentWindow ShowDropDown(Component component, Rect? rect = null, bool showPinClose = true)
        {
            if (component == null) return null;
            if (autoPopupWindow != null)
            {
                autoPopupWindow.Close();
                autoPopupWindow = null;
            }

            ComponentWindow wnd = CreateInstance<ComponentWindow>();
            wnd.component = component;

            if (!rect.HasValue)
            {
                Vector2 position = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
                Vector2 size = Prefs.contextMenuWindowSize;
                rect = new Rect(position - size / 2, size);
            }

            Rect r = rect.Value;
            if (r.y < 30) r.y = 30;
            wnd.position = r
;
            wnd.ShowPopup();
            wnd.Focus();
            wnd.adjustHeight = 1;
            wnd.isPopup = true;
            if (showPinClose)
            {
                PinAndClose.Show(wnd, r
, wnd.Close, () =>
                {
                    Show(component).position = wnd.position;
                    wnd.Close();
                });
            }
            else
            {
                autoPopupWindow = wnd;
                wnd.isAutoPopup = true;
                wnd.binding = EventManager.AddBinding(EventManager.ClosePopupEvent);
                wnd.binding.OnInvoke += b =>
                {
                    wnd.Close();
                    b.Remove();
                };
            }
            
            return wnd;
        }

        public static ComponentWindow ShowUtility(Component component)
        {
            if (component == null) return null;

            ComponentWindow wnd = CreateInstance<ComponentWindow>();
            wnd.titleContent = new GUIContent(component.GetType().Name + " (" + component.gameObject.name + ")");
            wnd.component = component;
            wnd.ShowUtility();
            wnd.Focus();
            wnd.adjustHeight = 2;
            Vector2 size = Prefs.contextMenuWindowSize;
            wnd.position = new Rect(GUIUtility.GUIToScreenPoint(Event.current.mousePosition) - size / 2, size);
            return wnd;
        }

        private void StartDelayedClose()
        {
            if (delayedCloseStarted) return;

            delayedCloseStarted = true;
            delayedCloseStartTime = DateTime.Now.Ticks;
            EditorApplication.update -= WaitForClose;
            EditorApplication.update += WaitForClose;
        }

        private void StopDelayedClose()
        {
            delayedCloseStarted = false;
            EditorApplication.update -= WaitForClose;
        }

        private void TryRestoreComponent()
        {
            FreeEditor();
            _component = EditorUtility.InstanceIDToObject(componentID) as Component;
            if (_component != null)
            {
                if (type == _component.GetType().AssemblyQualifiedName)
                {
                    isMissedComponent = false;
                    ignoreNextRepaint = true;
                    return;
                }

                _component = null;
            }

            GameObject go = GameObject.Find(path);
            if (go == null)
            {
                isMissedComponent = true;
                ignoreNextRepaint = true;
                return;
            }

            Type t = Type.GetType(type);
            _component = go.GetComponent(t);
            isMissedComponent = _component == null;
            if (isMissedComponent) ignoreNextRepaint = true;
            else componentID = component.GetInstanceID();
        }

        public bool TryRestoreTerrainEditor()
        {
            if (Selection.activeGameObject == _component.gameObject)
            {
                InitEditor(_component);
                if (editor != null) return true;
            }
            
            EditorGUILayout.HelpBox("Select Terrain GameObject", MessageType.Info);
            if (GUILayout.Button("Select"))
            {
                Selection.activeGameObject = _component.gameObject;
            }

            return false;
        }

        private void WaitForClose()
        {
            if (new TimeSpan(DateTime.Now.Ticks - delayedCloseStartTime).TotalSeconds > 10)
            {
                EditorApplication.update -= WaitForClose;
                delayedCloseStarted = false;
                Close();
            }
        }
    }
}