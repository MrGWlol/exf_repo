﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using InfinityCode.uContext.JSON;
using InfinityCode.uContext.Tools;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InfinityCode.uContext.Windows
{
    [InitializeOnLoad]
    public class SceneHistory: EditorWindow
    {
        private const string FILENAME = "SceneHistory.json"; 

        public static List<SceneRecord> records;

        private static GUIContent closeContent;
        private static GUIContent showContent;
        private static GUIStyle showContentStyle;

        private Vector2 scrollPosition;
        private string filter = "";
        private List<SceneRecord> filteredRecords;
        private SceneRecord selectedItem;
        private int selectedIndex = 0;
        private double lastClickTime;

        static SceneHistory()
        {
            EditorSceneManager.sceneClosed += OnSceneClosed;
            Load();
        }

        private bool DrawRecord(SceneRecord record)
        {
            bool ret = false;
            EditorGUILayout.BeginHorizontal(selectedItem == record ? Styles.selectedRow : Styles.transparentRow);

            if (GUILayout.Button(showContent, showContentStyle, GUILayout.ExpandWidth(false), GUILayout.Height(12)))
            {
                EditorSceneManager.OpenScene(record.path);
                Close();
            }

            GUIContent content = new GUIContent(record.name, record.path);
            ButtonEvent ev = GUILayoutUtils.Button(content, EditorStyles.label, GUILayout.Height(20), GUILayout.MaxWidth(position.width - 30));

            ProcessRecordEvents(ev, record);

            if (GUILayout.Button(closeContent, Styles.transparentButton, GUILayout.ExpandWidth(false), GUILayout.Height(12))) ret = true;

            EditorGUILayout.EndHorizontal();

            return ret;
        }

        private static void Load()
        {
            records = new List<SceneRecord>();

            if (!File.Exists(FILENAME)) return;

            string json = File.ReadAllText(FILENAME, Encoding.UTF8);
            records = Json.Deserialize<List<SceneRecord>>(json);
        }

        private void OnDestroy()
        {
            filteredRecords = null;
            filter = "";
        }

        private void OnEnable()
        {
            if (filteredRecords == null && records.Count > 0) selectedItem = records[selectedIndex];

            showContent = new GUIContent(Styles.isProSkin ? Icons.openNewWhite : Icons.openNewBlack, "Open Scene");
            closeContent = new GUIContent(Styles.isProSkin ? Icons.closeWhite : Icons.closeBlack, "Remove");
        }

        private void OnGUI()
        {
            if (showContentStyle == null || showContentStyle.normal.background == null)
            {
                showContentStyle = new GUIStyle(Styles.transparentButton)
                {
                    margin =
                    {
                        top = 6,
                        left = 6
                    }
                };
            }

            if (ProcessEvents())
            {
                Close();
                return;
            }
            Toolbar();

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            int removeIndex = -1;

            if (filteredRecords == null)
            {
                for (int i = 0; i < records.Count; i++)
                {
                    SceneRecord record = records[i];
                    if (DrawRecord(record)) removeIndex = i;
                }
            }
            else
            {
                for (int i = 0; i < filteredRecords.Count; i++)
                {
                    SceneRecord record = filteredRecords[i];
                    if (DrawRecord(record)) removeIndex = records.IndexOf(record);
                }
            }

            if (removeIndex != -1)
            {
                records.RemoveAt(removeIndex);
                Save();
                if (filteredRecords != null) UpdateFilteredRecords();
            }

            EditorGUILayout.EndScrollView();
        }

        private static void OnSceneClosed(Scene scene)
        {
            SelectionHistory.Clear();
            string path = scene.path;
            if (string.IsNullOrEmpty(path) || records.Any(r => r.path == path)) return;

            records.Insert(0, new SceneRecord
            {
                path = path,
                name = scene.name
            });

            while (records.Count > 19) records.RemoveAt(records.Count - 1);
            Save();
        }

        [MenuItem("File/Recent Scenes %#o", false, 155)]
        public static void OpenWindow()
        {
            GetWindow<SceneHistory>(true, "Recent Scenes");
        }

        private bool ProcessEvents()
        {
            Event e = Event.current;
            if (e.type == EventType.KeyDown)
            {
                if (e.keyCode == KeyCode.DownArrow) SelectNext();
                else if (e.keyCode == KeyCode.UpArrow) SelectPrev();
                else if (e.keyCode == KeyCode.KeypadEnter || e.keyCode == KeyCode.Return) return SelectCurrent();
            }

            return false;
        }

        private void ProcessRecordClick(SceneRecord record, Event e)
        {
            if (selectedItem == record)
            {
                if (EditorApplication.timeSinceStartup - lastClickTime < 0.3)
                {
                    SelectCurrent();
                    Close();
                    return;
                }

                lastClickTime = EditorApplication.timeSinceStartup;
            }
            else
            {
                lastClickTime = EditorApplication.timeSinceStartup;
                selectedItem = record;
                selectedIndex = records.IndexOf(record);
            }

            e.Use();
        }

        private void ProcessRecordEvents(ButtonEvent ev, SceneRecord record)
        {
            Event e = Event.current;
            if (ev == ButtonEvent.click)
            {
                if (e.button == 0) ProcessRecordClick(record, e);
            }
        }

        private static void Save()
        {
            File.WriteAllText(FILENAME, Json.Serialize(records).ToString(), Encoding.UTF8);
        }

        private bool SelectCurrent()
        {
            if (selectedItem != null)
            {
                SelectionHistory.Clear();
                EditorSceneManager.OpenScene(selectedItem.path);
                return true;
            }

            return false;
        }

        private void SelectPrev()
        {
            selectedIndex--;
            if (filteredRecords != null)
            {
                if (filteredRecords.Count == 0)
                {
                    selectedIndex = 0;
                    selectedItem = null;
                }
                else
                {
                    if (selectedIndex < 0) selectedIndex = filteredRecords.Count - 1;
                    selectedItem = filteredRecords[selectedIndex];
                }
            }
            else
            {
                if (selectedIndex < 0) selectedIndex = records.Count - 1;
                selectedItem = records[selectedIndex];
            }
            Event.current.Use();
            Repaint();
        }

        private void SelectNext()
        {
            selectedIndex++;
            if (filteredRecords != null)
            {
                if (filteredRecords.Count == 0)
                {
                    selectedIndex = 0;
                    selectedItem = null;
                }
                else
                {
                    if (selectedIndex >= filteredRecords.Count) selectedIndex = 0;
                    selectedItem = filteredRecords[selectedIndex];
                }
            }
            else
            {
                if (selectedIndex >= records.Count) selectedIndex = 0;
                selectedItem = records[selectedIndex];
            }

            Event.current.Use();
            Repaint();
        }

        private void Toolbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            EditorGUI.BeginChangeCheck();
            filter = EditorReflected.ToolbarSearchField(filter);
            if (EditorGUI.EndChangeCheck()) UpdateFilteredRecords();

            EditorGUILayout.EndHorizontal();
        }

        private void UpdateFilteredRecords()
        {
            if (string.IsNullOrEmpty(filter))
            {
                filteredRecords = null;
                return;
            }

            string assetType;
            string pattern = SearchableItem.GetPattern(filter, out assetType);

            filteredRecords = records.Where(i => i.UpdateAccuracy(pattern) > 0).OrderByDescending(i => i.accuracy).ToList();
            if (!filteredRecords.Contains(selectedItem))
            {
                if (filteredRecords.Count > 0) selectedItem = filteredRecords[0];
                else selectedItem = null;
                selectedIndex = 0;
            }
            else
            {
                selectedIndex = filteredRecords.IndexOf(selectedItem);
            }
        }

        [MenuItem("File/Recent Scenes %#o", true, 155)]
        public static bool ValidateOpenWindow()
        {
            return records.Count > 0;
        }

        public class SceneRecord : SearchableItem
        {
            public string name;
            public string path;

            protected override string[] GetSearchStrings()
            {
                return new[] { name };
            }
        }
    }
}