﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Windows
{
    public partial class CreateBrowser
    {
        public abstract class Item : SearchableItem
        {
            internal FolderItem parent;
            internal string label;
            private string[] _search;
            protected GUIContent _content;

            internal GUIContent content
            {
                get
                {
                    if (_content == null) InitContent();
                    return _content;
                }
            }

            protected abstract void InitContent();

            public virtual void Draw()
            {
                EditorGUILayout.BeginHorizontal(selectedItem == this ? Styles.selectedRow : Styles.transparentRow);
                if (GUILayout.Button(content, EditorStyles.label, GUILayout.Height(18)))
                {
                    if (Event.current.button == 0) OnClick();
                    else
                    {
                        selectedItem = this;
                        instance.UpdateSelectedIndex();
                    }
                    if (instance != null) instance.Repaint();
                }
                EditorGUILayout.EndHorizontal();
            }

            public virtual void Filter(string pattern, List<Item> filteredItems)
            {
                if (UpdateAccuracy(pattern) > 0) filteredItems.Add(this);
            }

            protected override string[] GetSearchStrings()
            {
                if (_search == null) _search = new[] {label};
                return _search;
            }

            public abstract void OnClick();

            public virtual void Dispose()
            {
                _content = null;
                parent = null;
            }
        }
    }
}