﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Windows
{
    public partial class CreateBrowser
    {
        internal class PrefabItem : Item
        {
            public string path;

            public PrefabItem(string label, string path)
            {
                if (label.Length < 8) return;

                this.label = label.Substring(0, label.Length - 7);
                this.path = path;
            }

            public override void Dispose()
            {
                base.Dispose();

                path = null;
                previewEditor = null;
            }

            public override void Draw()
            {
                if (content.image == null)
                {
                    GameObject asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    content.image = AssetPreview.GetAssetPreview(asset);
                    if (content.image == null) content.image = AssetPreview.GetMiniThumbnail(asset);
                }

                EditorGUILayout.BeginHorizontal(selectedItem == this ? Styles.selectedRow : Styles.transparentRow);

                GUILayout.Box(content, EditorStyles.label, GUILayout.Height(18));
                Event e = Event.current;

                if (e.type == EventType.MouseDown && GUILayoutUtility.GetLastRect().Contains(e.mousePosition))
                {
                    if (e.button == 0) OnClick();
                    else
                    {
                        selectedItem = this;
                        instance.UpdateSelectedIndex();
                        instance.Repaint();
                    }
                    e.Use();
                }

                EditorGUILayout.EndHorizontal();
            }

            public void DrawPreview()
            {
                if (previewPrefab != this && previewEditor != null)
                {
                    DestroyImmediate(previewEditor);
                }

                if (previewEditor == null)
                {
                    previewPrefab = this;
                    previewEditor = Editor.CreateEditor(AssetDatabase.LoadAssetAtPath<GameObject>(path));
                }

                if (previewEditor != null) previewEditor.OnInteractivePreviewGUI(GUILayoutUtility.GetRect(128, 128), Styles.grayRow);
            }

            protected override void InitContent()
            {
                _content = new GUIContent(label);
            }

            public override void OnClick()
            {
                if (instance.OnSelectPrefab != null) instance.OnSelectPrefab(path);
                instance.Close();
            }
        }
    }
}