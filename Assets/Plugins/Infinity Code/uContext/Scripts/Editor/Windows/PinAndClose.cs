﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System;
using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext.Windows
{
    public class PinAndClose : PopupWindow
    {
        private GUIContent _tabContent;
        private GUIContent _closeContent;
        private Action OnPin;
        private Action OnClose;
        private static EventManager.EventBinding binding;
        private EditorWindow targetWindow;
        private bool waitRestoreAfterPicker;

        private GUIContent tabContent
        {
            get
            {
                if (_tabContent == null) _tabContent = new GUIContent(Icons.pin, "To Tab Window"); 
                return _tabContent;
            }
        }

        private GUIContent closeContent
        {
            get
            {
                if (_closeContent == null) _closeContent = new GUIContent(Icons.closeWhite, "Close");
                return _closeContent;
            }
        }

        private bool InvokeClose()
        {
            if (OnClose != null) OnClose();
            Close();
            return true;
        }

        private void InvokePin()
        {
            OnPin();
            Close();
        }

        protected void OnDestroy()
        {
            OnPin = null;
            OnClose = null;

            targetWindow = null;

            if (binding != null) binding.Remove();
        }

        protected override void OnGUI()
        {
            try
            {
                if (targetWindow == null)
                {
                    Close();
                    return;
                }

                if (focusedWindow != this && focusedWindow != targetWindow)
                {
                    if (waitRestoreAfterPicker && (focusedWindow == null || focusedWindow.GetType() != EditorTypes.objectSelector))
                    {
                        targetWindow.Focus();
                        waitRestoreAfterPicker = false;
                    }
                    else if (TryToClose()) return;
                }

                base.OnGUI();

                EditorGUILayout.BeginHorizontal();

                if (OnPin != null && GUILayout.Button(tabContent, Styles.transparentButton, GUILayout.Width(12), GUILayout.Height(12))) InvokePin();
                if (GUILayout.Button(closeContent, Styles.transparentButton, GUILayout.Width(12), GUILayout.Height(12))) InvokeClose();

                EditorGUILayout.EndHorizontal();
            }
            catch (Exception e)
            {
                Log.Add(e);
            }

            Repaint();
        }

        public static PinAndClose Show(EditorWindow window, Rect inspectorRect, Action OnClose, Action OnLock = null)
        {
            PinAndClose wnd = CreateInstance<PinAndClose>();
            wnd.minSize = new Vector2(20, 20);
            wnd.targetWindow = window;
            wnd.OnClose = OnClose;
            wnd.OnPin = OnLock;
            Vector2 size = new Vector2(OnLock != null? 36: 20, 20);
            Vector2 position = inspectorRect.position + new Vector2(inspectorRect.width, 0) - size;
            wnd.position = new Rect(position, size);
            wnd.ShowPopup();
            wnd.Focus();
            window.Focus();

            binding = EventManager.AddBinding(EventManager.ClosePopupEvent);
            binding.OnInvoke += b =>
            {
                wnd.InvokeClose();
                b.Remove();
            };

            return wnd;
        }

        private bool TryToClose()
        {
            if (DragAndDrop.objectReferences != null && DragAndDrop.objectReferences.Length > 0) return false;
            if (focusedWindow != null && focusedWindow.GetType() == EditorTypes.objectSelector)
            {
                waitRestoreAfterPicker = true;
                return false;
            }
            return InvokeClose();
        }

        public void UpdatePosition(Rect rect)
        {
            Rect r = position;
            Vector2 size = r.size;
            Vector2 pos = rect.position + new Vector2(rect.width, 0) - size;
            r.position = pos;
            r.size = size;
            position = r;
        }
    }
}