﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using UnityEditor;

namespace InfinityCode.uContext
{
    public static partial class Prefs
    {
        internal static DuplicateToolManager duplicateToolManager = new DuplicateToolManager();

        public static bool switchToDuplicateToolByHotKey = true;

        public class DuplicateToolManager : PrefManager
        {
            public override IEnumerable<string> keywords
            {
                get { return new[] { "Switch to Duplicate Tool By HotKey" }; }
            }

            public override float order
            {
                get { return -11f; }
            }

            public override void Draw()
            {
#if !UCONTEXT_PRO
                switchToDuplicateToolByHotKey = EditorGUILayout.ToggleLeft("Switch to Duplicate Tool By HotKey (PRO)", switchToDuplicateToolByHotKey, EditorStyles.boldLabel);
#else
                switchToDuplicateToolByHotKey = EditorGUILayout.ToggleLeft("Switch to Duplicate Tool By HotKey", switchToDuplicateToolByHotKey, EditorStyles.boldLabel);
#endif
            }
        }
    }
}