﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using UnityEditor;

namespace InfinityCode.uContext
{
    public static partial class Prefs
    {
        internal static ToolValuesManager toolValuesManager = new ToolValuesManager();

        public static bool showToolValues = true;

        public class ToolValuesManager : PrefManager
        {
            public override IEnumerable<string> keywords
            {
                get
                {
                    return new[]
                    {
                        "Show Tool Values In Scene View"
                    };
                }
            }

            public override float order
            {
                get { return -9f; }
            }

            public override void Draw()
            {
                showToolValues = EditorGUILayout.ToggleLeft("Show Tool Values In SceneView", showToolValues, EditorStyles.boldLabel);
            }
        }
    }
}