﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections.Generic;
using UnityEditor;

namespace InfinityCode.uContext
{
    public static partial class Prefs
    {
        public static bool actions = true;
        public static bool actionsAddComponent = false;

        internal static ActionsManager actionsManager = new ActionsManager();

        internal class ActionsManager : PrefManager
        {
            public override IEnumerable<string> keywords
            {
                get { return new[] { "Actions", "Add Component Action For Single GameObject In Actions" }; }
            }

            public override float order
            {
                get { return -80; }
            }

            public override void Draw()
            {
                EditorGUI.indentLevel++;
                actions = EditorGUILayout.ToggleLeft("Actions", actions, EditorStyles.boldLabel);

                EditorGUI.BeginDisabledGroup(!actions);
                EditorGUI.indentLevel++;
                actionsAddComponent = EditorGUILayout.ToggleLeft("Add Component Action For Single GameObject In Actions", actionsAddComponent);
                EditorGUI.indentLevel--;
                EditorGUI.EndDisabledGroup();
                EditorGUI.indentLevel--;
            }
        }
    }
}