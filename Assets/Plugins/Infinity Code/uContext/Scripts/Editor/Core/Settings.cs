﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using UnityEditor;

namespace InfinityCode.uContext
{
    public static class Settings
    {
        [SettingsProvider]
        public static SettingsProvider GetSettingsProvider()
        {
            SettingsProvider provider = new SettingsProvider("Project/uContext", SettingsScope.Project)
            {
                label = "uContext",
                guiHandler = Prefs.OnGUI,
                keywords = Prefs.GetKeywords()
            };
            return provider;
        }

        [MenuItem(WindowsHelper.MenuPath + "Settings", false, 122)]
        public static void OpenSettings()
        {
            SettingsService.OpenProjectSettings("Project/uContext");
        }
    }
}