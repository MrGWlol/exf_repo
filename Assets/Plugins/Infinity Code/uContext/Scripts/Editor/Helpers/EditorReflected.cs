﻿/*           INFINITY CODE          */
/*     https://infinity-code.com    */

using System.Collections;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace InfinityCode.uContext
{
    public static class EditorReflected
    {
        private static MethodInfo toolbarSearchFieldMethod;

        public static IList GetGameViews()
        {
#if UNITY_2019_3_OR_NEWER
            IList list = Reflection.GetStaticFieldValue<IList>(EditorTypes.playModeView, "s_PlayModeViews");
#else
            IList list = Reflection.GetStaticFieldValue<IList>(EditorTypes.gameView, "s_GameViews");
#endif
            return list;
        }

        public static string ToolbarSearchField(string value)
        {
            if (toolbarSearchFieldMethod == null)
            {
                toolbarSearchFieldMethod = typeof(EditorGUILayout).GetMethod("ToolbarSearchField", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new[] { typeof(string), typeof(GUILayoutOption[]) }, null);
            }
            if (toolbarSearchFieldMethod != null) return toolbarSearchFieldMethod.Invoke(null, new object[] { value, null }) as string;
            return value;
        }
    }
}